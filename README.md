# MaterialColorGenerator

Generate a full list of colors to use with a Jetpack Compose Theme.

## Usage

In [Main.kt](src/Main.kt), update the seed variable with the hexadecimal ARGB format `0xFFFFFFFF` and run the main function.
You can optionally add a package name and a file path including file name and extension.

```kotlin
FileColorGenerator.generate(
    seed = 0xFF2196F3,
    filePath = "./Color.kt",
    packageName = "com.example.compose"
)
```

## About

This color generator is based on the [material-color-utilities](https://github.com/material-foundation/material-color-utilities) 
and match the latest guidelines provided by the [material specifications](https://m3.material.io/styles/color/static/baseline).

## License

[Apache License 2.0](LICENSE)