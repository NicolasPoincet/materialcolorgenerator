package model

import materialcolorutilities.palettes.CorePalette
import palette.DarkColorPalette
import palette.LightColorPalette

data class Theme(
    val seed: Int,
) {

    private val core by lazy { CorePalette.of(seed) }

    val lightColorPalette by lazy {
        LightColorPalette(
            primary = core.primary.tone(40),
            onPrimary = core.primary.tone(100),
            primaryContainer = core.primary.tone(90),
            onPrimaryContainer = core.primary.tone(10),

            secondary = core.secondary.tone(40),
            onSecondary = core.secondary.tone(100),
            secondaryContainer = core.secondary.tone(90),
            onSecondaryContainer = core.secondary.tone(10),

            tertiary = core.tertiary.tone(40),
            onTertiary = core.tertiary.tone(100),
            tertiaryContainer = core.tertiary.tone(90),
            onTertiaryContainer = core.tertiary.tone(10),

            error = core.error.tone(40),
            onError = core.error.tone(100),
            errorContainer = core.error.tone(90),
            onErrorContainer = core.error.tone(10),

            surface = core.neutral.tone(98),
            onSurface = core.neutral.tone(10),
            surfaceVariant = core.neutralVariant.tone(90),
            onSurfaceVariant = core.neutralVariant.tone(30),
            surfaceContainerHighest = core.neutral.tone(90),
            surfaceContainerHigh = core.neutral.tone(92),
            surfaceContainer = core.neutral.tone(94),
            surfaceContainerLow = core.neutral.tone(96),
            surfaceContainerLowest = core.neutral.tone(100),
            inverseSurface = core.neutral.tone(20),
            inverseOnSurface = core.neutral.tone(95),
            surfaceTint = core.primary.tone(40),

            outline = core.neutralVariant.tone(50),
            outlineVariant = core.neutralVariant.tone(80),

            primaryFixed = core.primary.tone(90),
            onPrimaryFixed = core.primary.tone(10),
            primaryFixedDim = core.primary.tone(80),
            onPrimaryFixedVariant = core.primary.tone(30),
            inversePrimary = core.primary.tone(80),

            secondaryFixed = core.secondary.tone(90),
            onSecondaryFixed = core.secondary.tone(10),
            secondaryFixedDim = core.secondary.tone(80),
            onSecondaryFixedVariant = core.secondary.tone(30),

            tertiaryFixed = core.tertiary.tone(90),
            onTertiaryFixed = core.tertiary.tone(10),
            tertiaryFixedDim = core.tertiary.tone(80),
            onTertiaryFixedVariant = core.tertiary.tone(30),

            background = core.neutral.tone(98),
            onBackground = core.neutral.tone(10),
            surfaceBright = core.neutral.tone(98),
            surfaceDim = core.neutral.tone(87),
            shadow = core.neutral.tone(0),
            scrim = core.neutral.tone(0),
        )
    }

    val darkColorPalette by lazy {
        DarkColorPalette(
            primary = core.primary.tone(80),
            onPrimary = core.primary.tone(20),
            primaryContainer = core.primary.tone(30),
            onPrimaryContainer = core.primary.tone(90),

            secondary = core.secondary.tone(80),
            onSecondary = core.secondary.tone(20),
            secondaryContainer = core.secondary.tone(30),
            onSecondaryContainer = core.secondary.tone(90),

            tertiary = core.tertiary.tone(80),
            onTertiary = core.tertiary.tone(20),
            tertiaryContainer = core.tertiary.tone(30),
            onTertiaryContainer = core.tertiary.tone(90),

            error = core.error.tone(80),
            onError = core.error.tone(20),
            errorContainer = core.error.tone(30),
            onErrorContainer = core.error.tone(80),

            surface = core.neutral.tone(6),
            onSurface = core.neutral.tone(90),
            surfaceVariant = core.neutralVariant.tone(30),
            onSurfaceVariant = core.neutralVariant.tone(80),
            surfaceContainerHighest = core.neutral.tone(22),
            surfaceContainerHigh = core.neutral.tone(17),
            surfaceContainer = core.neutral.tone(12),
            surfaceContainerLow = core.neutral.tone(10),
            surfaceContainerLowest = core.neutral.tone(4),
            inverseSurface = core.neutral.tone(90),
            inverseOnSurface = core.neutral.tone(20),
            surfaceTint = core.primary.tone(80),

            outline = core.neutralVariant.tone(60),
            outlineVariant = core.neutralVariant.tone(30),

            primaryFixed = core.primary.tone(90),
            onPrimaryFixed = core.primary.tone(10),
            primaryFixedDim = core.primary.tone(80),
            onPrimaryFixedVariant = core.primary.tone(30),
            inversePrimary = core.primary.tone(40),

            secondaryFixed = core.secondary.tone(90),
            onSecondaryFixed = core.secondary.tone(10),
            secondaryFixedDim = core.secondary.tone(80),
            onSecondaryFixedVariant = core.secondary.tone(30),

            tertiaryFixed = core.tertiary.tone(90),
            onTertiaryFixed = core.tertiary.tone(10),
            tertiaryFixedDim = core.tertiary.tone(80),
            onTertiaryFixedVariant = core.tertiary.tone(30),

            background = core.neutral.tone(6),
            onBackground = core.neutral.tone(90),
            surfaceBright = core.neutral.tone(24),
            surfaceDim = core.neutral.tone(6),
            shadow = core.neutral.tone(0),
            scrim = core.neutral.tone(0),
        )
    }
}