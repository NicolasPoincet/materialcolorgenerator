package palette

interface ColorPalette {
    // Primary
    val primary: Int
    val onPrimary: Int
    val primaryContainer: Int
    val onPrimaryContainer: Int

    // Secondary
    val secondary: Int
    val onSecondary: Int
    val secondaryContainer: Int
    val onSecondaryContainer: Int

    // Tertiary
    val tertiary: Int
    val onTertiary: Int
    val tertiaryContainer: Int
    val onTertiaryContainer: Int

    // Error
    val error: Int
    val onError: Int
    val errorContainer: Int
    val onErrorContainer: Int

    // Surface
    val surface: Int
    val onSurface: Int
    val surfaceVariant: Int
    val onSurfaceVariant: Int
    val surfaceContainerHighest: Int
    val surfaceContainerHigh: Int
    val surfaceContainer: Int
    val surfaceContainerLow: Int
    val surfaceContainerLowest: Int
    val inverseSurface: Int
    val inverseOnSurface: Int
    val surfaceTint: Int

    // Outline
    val outline: Int
    val outlineVariant: Int

    // Add-ons Primary
    val primaryFixed: Int
    val onPrimaryFixed: Int
    val primaryFixedDim: Int
    val onPrimaryFixedVariant: Int
    val inversePrimary: Int

    // Add-ons Secondary
    val secondaryFixed: Int
    val onSecondaryFixed: Int
    val onSecondaryFixedVariant: Int
    val secondaryFixedDim: Int

    // Add-ons Tertiary
    val tertiaryFixed: Int
    val onTertiaryFixed: Int
    val onTertiaryFixedVariant: Int
    val tertiaryFixedDim: Int

    // Add-ons Surface
    val background: Int
    val onBackground: Int
    val surfaceBright: Int
    val surfaceDim: Int
    val scrim: Int
    val shadow: Int
}