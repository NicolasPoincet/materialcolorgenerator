fun main() {
    FileColorGenerator.generate(
        seed = 0xFF2196F3,
        filePath = "./Color.kt",
        packageName = "com.example.compose"
    )
}
