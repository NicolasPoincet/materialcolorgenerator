import model.Theme
import palette.ColorPalette
import utils.hexColorLong
import java.io.File
import java.io.PrintWriter

object FileColorGenerator {

    /**
     * Generate file with all colors
     * @param seed Seed color with the format 0xFFFFFFFF
     * @param filePath File path containing the file name and the extension.
     * @param packageName Package name to write on top of the file. If null or empty, the generated file will not contain the package line
     */
    fun generate(
        seed: Long,
        filePath: String = "./Color.kt",
        packageName: String? = "com.example.compose",
    ) {
        try {
            val file = File(filePath)
            file.createNewFile()
            val writer = file.printWriter()

            val themeSeed = seed.toInt()

            packageName.takeUnless { it.isNullOrEmpty() }?.let {
                writer.println("package $it")
            }
            writer.println("import androidx.compose.ui.graphics.Color")
            writer.println()
            writer.println("val seed = Color(${themeSeed.hexColorLong})")
            writer.println()

            val theme = Theme(themeSeed)

            writeTheme(
                writer = writer,
                palette = theme.lightColorPalette,
                prefix = "light"
            )
            writer.println()
            writeTheme(
                writer = writer,
                palette = theme.darkColorPalette,
                prefix = "dark"
            )
            writer.close()

            println(filePath)
            println("Colors successfully generated")
        } catch (e: Exception) {
            println("Error during color generation: ${e.message}")
        }
    }

    /**
     * Write all colors in the
     * @param writer PrintWriter to write content in file
     * @param palette Palette of colors
     * @param prefix Prefix colors name
     */
    private fun writeTheme(
        writer: PrintWriter,
        palette: ColorPalette,
        prefix: String,
    ) {
        writer.println("val md_theme_${prefix}_primary = Color(${palette.primary.hexColorLong})")
        writer.println("val md_theme_${prefix}_onPrimary = Color(${palette.onPrimary.hexColorLong})")
        writer.println("val md_theme_${prefix}_primaryContainer = Color(${palette.primaryContainer.hexColorLong})")
        writer.println("val md_theme_${prefix}_onPrimaryContainer = Color(${palette.onPrimaryContainer.hexColorLong})")
        writer.println("val md_theme_${prefix}_secondary = Color(${palette.secondary.hexColorLong})")
        writer.println("val md_theme_${prefix}_onSecondary = Color(${palette.onSecondary.hexColorLong})")
        writer.println("val md_theme_${prefix}_secondaryContainer = Color(${palette.secondaryContainer.hexColorLong})")
        writer.println("val md_theme_${prefix}_onSecondaryContainer = Color(${palette.onSecondaryContainer.hexColorLong})")
        writer.println("val md_theme_${prefix}_tertiary = Color(${palette.tertiary.hexColorLong})")
        writer.println("val md_theme_${prefix}_onTertiary = Color(${palette.onTertiary.hexColorLong})")
        writer.println("val md_theme_${prefix}_tertiaryContainer = Color(${palette.tertiaryContainer.hexColorLong})")
        writer.println("val md_theme_${prefix}_onTertiaryContainer = Color(${palette.onTertiaryContainer.hexColorLong})")
        writer.println("val md_theme_${prefix}_error = Color(${palette.error.hexColorLong})")
        writer.println("val md_theme_${prefix}_onError = Color(${palette.onError.hexColorLong})")
        writer.println("val md_theme_${prefix}_errorContainer = Color(${palette.errorContainer.hexColorLong})")
        writer.println("val md_theme_${prefix}_onErrorContainer = Color(${palette.onErrorContainer.hexColorLong})")
        writer.println("val md_theme_${prefix}_surface = Color(${palette.surface.hexColorLong})")
        writer.println("val md_theme_${prefix}_onSurface = Color(${palette.onSurface.hexColorLong})")
        writer.println("val md_theme_${prefix}_surfaceVariant = Color(${palette.surfaceVariant.hexColorLong})")
        writer.println("val md_theme_${prefix}_onSurfaceVariant = Color(${palette.onSurfaceVariant.hexColorLong})")
        writer.println("val md_theme_${prefix}_surfaceContainerHighest = Color(${palette.surfaceContainerHighest.hexColorLong})")
        writer.println("val md_theme_${prefix}_surfaceContainerHigh = Color(${palette.surfaceContainerHigh.hexColorLong})")
        writer.println("val md_theme_${prefix}_surfaceContainer = Color(${palette.surfaceContainer.hexColorLong})")
        writer.println("val md_theme_${prefix}_surfaceContainerLow = Color(${palette.surfaceContainerLow.hexColorLong})")
        writer.println("val md_theme_${prefix}_surfaceContainerLowest = Color(${palette.surfaceContainerLowest.hexColorLong})")
        writer.println("val md_theme_${prefix}_inverseSurface = Color(${palette.inverseSurface.hexColorLong})")
        writer.println("val md_theme_${prefix}_inverseOnSurface = Color(${palette.inverseOnSurface.hexColorLong})")
        writer.println("val md_theme_${prefix}_surfaceTint = Color(${palette.surfaceTint.hexColorLong})")
        writer.println("val md_theme_${prefix}_outline = Color(${palette.outline.hexColorLong})")
        writer.println("val md_theme_${prefix}_outlineVariant = Color(${palette.outlineVariant.hexColorLong})")
        writer.println("val md_theme_${prefix}_primaryFixed = Color(${palette.primaryFixed.hexColorLong})")
        writer.println("val md_theme_${prefix}_onPrimaryFixed = Color(${palette.onPrimaryFixed.hexColorLong})")
        writer.println("val md_theme_${prefix}_primaryFixedDim = Color(${palette.primaryFixedDim.hexColorLong})")
        writer.println("val md_theme_${prefix}_onPrimaryFixedVariant = Color(${palette.onPrimaryFixedVariant.hexColorLong})")
        writer.println("val md_theme_${prefix}_inversePrimary = Color(${palette.inversePrimary.hexColorLong})")
        writer.println("val md_theme_${prefix}_secondaryFixed = Color(${palette.secondaryFixed.hexColorLong})")
        writer.println("val md_theme_${prefix}_onSecondaryFixed = Color(${palette.onSecondaryFixed.hexColorLong})")
        writer.println("val md_theme_${prefix}_secondaryFixedDim = Color(${palette.secondaryFixedDim.hexColorLong})")
        writer.println("val md_theme_${prefix}_onSecondaryFixedVariant = Color(${palette.onSecondaryFixedVariant.hexColorLong})")
        writer.println("val md_theme_${prefix}_tertiaryFixed = Color(${palette.tertiaryFixed.hexColorLong})")
        writer.println("val md_theme_${prefix}_onTertiaryFixed = Color(${palette.onTertiaryFixed.hexColorLong})")
        writer.println("val md_theme_${prefix}_tertiaryFixedDim = Color(${palette.tertiaryFixedDim.hexColorLong})")
        writer.println("val md_theme_${prefix}_onTertiaryFixedVariant = Color(${palette.onTertiaryFixedVariant.hexColorLong})")
        writer.println("val md_theme_${prefix}_background = Color(${palette.background.hexColorLong})")
        writer.println("val md_theme_${prefix}_onBackground = Color(${palette.onBackground.hexColorLong})")
        writer.println("val md_theme_${prefix}_surfaceBright = Color(${palette.surfaceBright.hexColorLong})")
        writer.println("val md_theme_${prefix}_surfaceDim = Color(${palette.surfaceDim.hexColorLong})")
        writer.println("val md_theme_${prefix}_shadow = Color(${palette.shadow.hexColorLong})")
        writer.println("val md_theme_${prefix}_scrim = Color(${palette.scrim.hexColorLong})")
    }
}