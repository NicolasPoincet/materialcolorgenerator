package utils

import materialcolorutilities.utils.ColorUtils

val Int.hexColor: String
    get() {
        val r = ColorUtils.redFromArgb(this)
        val g = ColorUtils.greenFromArgb(this)
        val b = ColorUtils.blueFromArgb(this)
        return String.format("#%02x%02x%02x", r, g, b)
    }

val Int.hexColorLong: String
    get() {
        val r = ColorUtils.redFromArgb(this)
        val g = ColorUtils.greenFromArgb(this)
        val b = ColorUtils.blueFromArgb(this)
        return String.format("0xFF%02x%02x%02x", r, g, b)
    }