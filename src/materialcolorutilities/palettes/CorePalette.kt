package materialcolorutilities.palettes

import materialcolorutilities.hct.Hct
import kotlin.math.max
import kotlin.math.min

/**
 * An intermediate concept between the key color for a UI theme, and a full color scheme. 5 sets of
 * tones are generated, all except one use the same hue as the key color, and all vary in chroma.
 */
class CorePalette private constructor(
    private val argb: Int,
    private val isContent: Boolean
) {
    private val hct by lazy { Hct.fromInt(argb) }

    val primary: TonalPalette
        get() = when {
            isContent -> TonalPalette.fromHueAndChroma(hct.hue, hct.chroma)
            else -> TonalPalette.fromHueAndChroma(hct.hue, max(48.0, hct.chroma))
        }

    val secondary: TonalPalette
        get() = when {
            isContent -> TonalPalette.fromHueAndChroma(hct.hue, hct.chroma / 3.0)
            else -> TonalPalette.fromHueAndChroma(hct.hue, 16.0)
        }
    val tertiary: TonalPalette
        get() = when {
            isContent -> TonalPalette.fromHueAndChroma(hct.hue + 60.0, hct.chroma / 2.0)
            else -> TonalPalette.fromHueAndChroma(hct.hue + 60.0, 24.0)
        }
    val neutral: TonalPalette
        get() = when {
            isContent -> TonalPalette.fromHueAndChroma(hct.hue, min(hct.chroma / 12.0, 4.0))
            else -> TonalPalette.fromHueAndChroma(hct.hue, 4.0)
        }
    val neutralVariant: TonalPalette
        get() = when {
            isContent -> TonalPalette.fromHueAndChroma(hct.hue, min(hct.chroma / 6.0, 8.0))
            else -> TonalPalette.fromHueAndChroma(hct.hue, 8.0)
        }
    val error: TonalPalette get() = TonalPalette.fromHueAndChroma(25.0, 84.0)

    companion object {
        /**
         * Create key tones from a color.
         *
         * @param argb ARGB representation of a color
         */
        fun of(argb: Int) = CorePalette(argb, false)

        /**
         * Create content key tones from a color.
         *
         * @param argb ARGB representation of a color
         */
        fun contentOf(argb: Int) = CorePalette(argb, true)
    }
}
